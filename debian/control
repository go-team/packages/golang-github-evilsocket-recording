Source: golang-github-evilsocket-recording
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-evilsocket-islazy-dev,
               golang-github-kr-binarydist-dev
Standards-Version: 4.5.1
Rules-Requires-Root: no
Homepage: https://github.com/bettercap/recording
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-evilsocket-recording
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-evilsocket-recording.git
XS-Go-Import-Path: github.com/bettercap/recording
Testsuite: autopkgtest-pkg-go

Package: golang-github-evilsocket-recording-dev
Architecture: all
Depends: golang-github-evilsocket-islazy-dev,
         golang-github-kr-binarydist-dev,
         ${misc:Depends}
Description: Allows reading and writing bettercap's session recordings
 A recording archive is a gzip file containing reference Session and Events
 JSON objects and their changes stored as patches in order to keep the file
 size as small as possible. Loading a session file implies generating all
 the frames starting from the reference one by iteratively applying those
 "state patches" until all recorded frames are stored in memory. This
 is done to allow, UI side, to skip forward to a specific frame index
 without all intermediate states being computed at runtime.
 .
 This package allows reading and writing bettercap's session recordings.
